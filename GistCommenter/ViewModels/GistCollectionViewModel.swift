//
//  GistCollectionViewModel.swift
//  GistCommenter
//
//  Created by Bruno Machado on 20/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation
import RxSwift

class GistCollectionViewModel {
  
  public let id: String
  public let title: String
  public let creationDate: String
  public let commentCount: Int
  public let fileText: String
  public var avatarURL: URL?
  
  init(with gist: Gist) {
    self.id = gist.id
    let gistName: String = gist.files.first?.fileName ?? "gist:\(gist.id)"
    self.title = "\(gist.owner.login) / \(gistName)"
    self.fileText = gist.files.first?.content.truncate(length: 300) ?? ""
    self.creationDate = gist.creationDate.beautifulDate()
    self.avatarURL = URL(string: gist.owner.avatarURL)
    self.commentCount = gist.commentCount
  }
}
