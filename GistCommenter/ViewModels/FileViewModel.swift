//
//  FileViewModel.swift
//  GistCommenter
//
//  Created by Bruno Machado on 20/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation

class FileViewModel {
  
  public let fileName: String
  public let content: String
  
  init(with file: File) {
    self.fileName = file.fileName
    self.content = file.content
  }
}
