//
//  CommentViewModel.swift
//  GistCommenter
//
//  Created by Bruno Machado on 20/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation

class CommentViewModel {
  
  public let userName: String
  public let creationDate: String
  public let body: String
  public let avatarURL: URL?
  
  init(with comment: Comment) {
    self.userName = comment.user.login
    self.creationDate = comment.creationDate.beautifulDate()
    self.body = comment.body
    self.avatarURL = URL(string: comment.user.avatarURL)
  }
}
