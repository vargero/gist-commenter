//
//  HomeViewModel.swift
//  GistCommenter
//
//  Created by Bruno Machado on 20/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation
import RxSwift

class HomeViewModel {
  
  public let disposeBag = DisposeBag()
  
  public let gistCollectionVariable = Variable<[GistCollectionViewModel]>([])
  
  public var gistCollection: [GistCollectionViewModel] {
    get {
      return gistCollectionVariable.value
    }
  }
  
  init() {
    update()
  }
  
  func update() {
    let observables = DefaultsManager.shared.gistList
      .compactMap { GistService.shared.requestGist(with: $0) }.reversed()
    Observable.from(observables).merge().toArray().subscribe(onNext: { [weak self] gists in
      let viewModelList = gists.compactMap { GistCollectionViewModel(with: $0) }
      self?.gistCollectionVariable.value = viewModelList
    }).disposed(by: disposeBag)
  }
}
