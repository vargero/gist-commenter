//
//  GistViewModel.swift
//  GistCommenter
//
//  Created by Bruno Machado on 20/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation
import RxSwift

class GistViewModel {
  
  private let disposeBag = DisposeBag()
  
  public let id: String
  public let avatarURL: URL?
  public let userName: String
  public let creationDate: String
  public let description: String
  public let files: [FileViewModel]
  public let commentsVariable = Variable<[CommentViewModel]>([])
  
  public var comments: [CommentViewModel] {
    get {
      return commentsVariable.value
    }
  }
  
  init(with gist: Gist) {
    self.id = gist.id
    self.userName = gist.owner.login
    self.description = gist.description
    self.creationDate = gist.creationDate.beautifulDate()
    self.avatarURL = URL(string: gist.owner.avatarURL)
    self.files = gist.files.compactMap { FileViewModel(with: $0) }
    
    refreshComments()
  }
  
  func refreshComments() {
    GistService.shared.requestGistComments(with: id).subscribe(onNext: { [weak self] comments in
      guard let me = self else { return }
      me.commentsVariable.value = comments.compactMap { CommentViewModel(with: $0) }.reversed()
    }).disposed(by: disposeBag)
  }
  
  func addComment(body: String) {
    GistService.shared.sendGistComment(with: id, body: body).subscribe(onNext: { [weak self] comment in
      guard let me = self else { return }
      me.commentsVariable.value.insert(CommentViewModel(with: comment), at: 0)
    }).disposed(by: disposeBag)
  }
}
