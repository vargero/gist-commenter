//
//  HomeViewController.swift
//  GistCommenter
//
//  Created by Bruno Machado on 18/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

protocol HomeViewControllerDelegate {
  func viewControllerDidSelectGist(_ viewController: HomeViewController, with gistId: String)
  func viewControllerDidTapCameraButton(_ viewController: HomeViewController)
}

class HomeViewController: UIViewController {
  
  private let homeView = HomeView()
  private let disposeBag = DisposeBag()
  private let viewModel = HomeViewModel()
  private let footerView = GistCollectionFooterView()
  
  var delegate: HomeViewControllerDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Recently Opened Gists"
    self.view = homeView
    setupCollectionView()
    
    homeView.button.rx.tap.asDriver().drive(onNext: { [weak self] _ in
      guard let me = self else { return }
      me.delegate?.viewControllerDidTapCameraButton(me)
    }).disposed(by: disposeBag)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    viewModel.update()
  }
  
  func setupCollectionView() {
    let collectionView = homeView.collectionView
    collectionView.backgroundColor = .lightGray
    
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    
    collectionView.register(GistCollectionViewCell.self,
                            forCellWithReuseIdentifier: GistCollectionViewCell.description())
    
    viewModel.gistCollectionVariable.asDriver().drive(onNext: { [weak self] _ in
      self?.homeView.collectionView.reloadData()
    }).disposed(by: disposeBag)
  }
}

extension HomeViewController: UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    delegate?.viewControllerDidSelectGist(self, with: viewModel.gistCollection[indexPath.item].id)
  }
}

extension HomeViewController: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return viewModel.gistCollection.count
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView
      .dequeueReusableCell(withReuseIdentifier: GistCollectionViewCell.description(), for: indexPath)
      as? GistCollectionViewCell else {
        return UICollectionViewCell()
    }
    
    cell.setup(with: viewModel.gistCollection[indexPath.item])
    
    return cell
  }
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {

  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {

    return CGSize(width: collectionView.bounds.width - 20, height: 250)
  }

  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 20
  }

  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }

  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
  }
}
