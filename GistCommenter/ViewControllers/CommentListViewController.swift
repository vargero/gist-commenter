//
//  CommentListViewController.swift
//  GistCommenter
//
//  Created by Bruno Machado on 19/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

protocol CommentListViewControllerDelegate {
  func viewControllerDidSendComment(_ viewController: CommentListViewController, comment: String)
}

class CommentListViewController: UIViewController {
  
  private let commentListView = CommentListView()
  private let viewModel: Variable<[CommentViewModel]>
  private let disposeBag = DisposeBag()
  public var delegate: CommentListViewControllerDelegate?
  
  init(viewModel: Variable<[CommentViewModel]>) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }
  
  @available(*, unavailable)
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view = commentListView
    
    commentListView.submitCommentButton.rx.tap.asObservable().subscribe(onNext: { [weak self] _ in
      guard let me = self,
        let comment = me.commentListView.commentField.text else { return }
      me.sendComment(comment)
      me.commentListView.commentField.resignFirstResponder()
      me.commentListView.commentField.text = nil
    }).disposed(by: disposeBag)
    
    setupTableView()
  }
  
  func dismissKeyboard() {
    commentListView.commentField.resignFirstResponder()
  }
  
  private func setupTableView() {
    let tableView = commentListView.commentTableView
    
    tableView.dataSource = self
    tableView.register(GistCommentTableViewCell.self,
                       forCellReuseIdentifier: GistCommentTableViewCell.description())
    
    viewModel.asDriver().drive(onNext: { [weak self] _ in
      self?.commentListView.commentTableView.reloadData()
    }).disposed(by: disposeBag)
  }
  
  private func sendComment(_ comment: String) {
    delegate?.viewControllerDidSendComment(self, comment: comment)
  }
}

extension CommentListViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.value.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: GistCommentTableViewCell.description()) as? GistCommentTableViewCell else {
      return UITableViewCell()
    }
    
    cell.setup(with: viewModel.value[indexPath.item])
    return cell
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
}
