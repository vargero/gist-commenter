//
//  GistDetailViewController.swift
//  GistCommenter
//
//  Created by Bruno Machado on 19/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import RxSwift
import UIKit

class GistDetailViewController: UIViewController {
  
  private let gistDetailView = GistDetailView()
  private lazy var commentListViewController = {
    return CommentListViewController(viewModel: viewModel.commentsVariable)
  }()
  private var internalGist: Gist?
  private var disposeBag = DisposeBag()
  
  private let viewModel: GistViewModel
  
  init(viewModel: GistViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }
  
  @available(*, unavailable)
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Gist"
    view = gistDetailView
    setupCommentsView()
    setupFileTableView()
    commentListViewController.delegate = self
    gistDetailView.setup(with: viewModel)
    
    viewModel.commentsVariable.asDriver().drive(onNext: { [weak self] _ in
      guard let me = self else { return }
      me.gistDetailView.setup(with: me.viewModel)
    }).disposed(by: disposeBag)
    
    gistDetailView.isShowingCommentContainerVariable.asDriver().drive(onNext: { [weak self] _ in
      self?.viewModel.refreshComments()
      self?.commentListViewController.dismissKeyboard()
    }).disposed(by: disposeBag)
  }
  
  private func setupFileTableView() {
    let tableView = gistDetailView.filesTableView
    
    tableView.separatorStyle = .none
    
    tableView.dataSource = self
    tableView.delegate = self
    tableView.register(FileCollectionViewCell.self,
                       forCellReuseIdentifier: FileCollectionViewCell.description())
  }
  
  private func setupCommentsView() {
    addChildViewController(commentListViewController)
    gistDetailView.commentContainerView.addSubview(commentListViewController.view)
    
    commentListViewController.view.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
    
    commentListViewController.didMove(toParentViewController: self)
  }
  
  private func updateComments() {
    viewModel.refreshComments()
  }
}

extension GistDetailViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: FileCollectionViewCell.description())
      as? FileCollectionViewCell else {
        return UITableViewCell()
    }
    
    cell.setup(with: viewModel.files[indexPath.row])
    return cell
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
   return viewModel.files.count
  }
}

extension GistDetailViewController: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerView = FileSectionHeaderView()
    headerView.setup(with: viewModel.files[section].fileName)
    return headerView
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 30.0
  }
}

extension GistDetailViewController: CommentListViewControllerDelegate {
  
  func viewControllerDidSendComment(_ viewController: CommentListViewController, comment: String) {
    viewModel.addComment(body: comment)
  }
}
