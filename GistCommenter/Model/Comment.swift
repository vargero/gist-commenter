//
//  Comment.swift
//  GistCommenter
//
//  Created by Bruno Machado on 19/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation

struct Comment {
  
  private enum Keys: String {
    case body
    case creationDate = "created_at"
    case id
    case updatedDate = "updated_at"
    case user
  }
  
  let id: Int64
  let body: String
  
  let user: User
  
  let creationDate: Date
  let updatedDate: Date
}

extension Comment: Serializable {
  init?(json: JSONPayload) {
    guard let id = json[Keys.id.rawValue] as? Int64,
      let body = json[Keys.body.rawValue] as? String,
      let rawCreationDate = json[Keys.creationDate.rawValue] as? String,
      let creationDate = DateFormatter.standardDateFormatter.date(from: rawCreationDate),
      let rawUpdatedDate = json[Keys.updatedDate.rawValue] as? String,
      let updatedDate = DateFormatter.standardDateFormatter.date(from: rawUpdatedDate),
      let rawUser = json[Keys.user.rawValue] as? JSONPayload,
      let user = User(json: rawUser) else {
        return nil
    }
    
    self.id = id
    self.body = body
    self.user = user
    self.creationDate = creationDate
    self.updatedDate = updatedDate
  }
}
