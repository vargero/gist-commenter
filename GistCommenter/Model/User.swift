//
//  User.swift
//  GistCommenter
//
//  Created by Bruno Machado on 18/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation

struct User {
  
  private enum Keys: String {
    case avatarURL = "avatar_url"
    case id
    case login
  }
  
  let avatarURL: String
  let id: Int64
  let login: String
}

extension User: Serializable {
  
  init?(json: JSONPayload) {
    guard let avatarURL = json[Keys.avatarURL.rawValue] as? String,
      let id = json[Keys.id.rawValue] as? Int64,
      let login = json[Keys.login.rawValue] as? String else {
        return nil
    }
    
    self.avatarURL = avatarURL
    self.id = id
    self.login = login
  }
}
