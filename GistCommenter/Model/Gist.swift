//
//  Gist.swift
//  GistCommenter
//
//  Created by Bruno Machado on 18/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation
import RxSwift

struct Gist {
  
  private enum Keys: String {
    case commentCount = "comments"
    case creationDate = "created_at"
    case description
    case files
    case id
    case owner
    case updatedDate = "updated_at"
  }
  
  let id: String
  
  let commentCount: Int
  let description: String
  
  let creationDate: Date
  let updatedDate: Date
  
  let files: [File]
  let owner: User
}

extension Gist: Serializable {
  public init?(json: JSONPayload) {
    guard let id = json[Keys.id.rawValue] as? String,
      let commentCount = json[Keys.commentCount.rawValue] as? Int,
      let description = json[Keys.description.rawValue] as? String,
      let rawCreationDate = json[Keys.creationDate.rawValue] as? String,
      let creationDate = DateFormatter.standardDateFormatter.date(from: rawCreationDate),
      let rawUpdatedDate = json[Keys.updatedDate.rawValue] as? String,
      let updatedDate = DateFormatter.standardDateFormatter.date(from: rawUpdatedDate),
      let rawOwner = json[Keys.owner.rawValue] as? JSONPayload,
      let owner = User(json: rawOwner),
      let rawFiles = json[Keys.files.rawValue] as? JSONPayload,
      let files = Gist.parseFiles(rawFiles) else {
      return nil
    }
    
    
    self.id = id
    
    self.commentCount = commentCount
    self.description = description
    
    self.creationDate = creationDate
    self.updatedDate = updatedDate
    
    self.owner = owner
    self.files = files
  }
  
  private static func parseFiles(_ rawFiles: JSONPayload) -> [File]? {
    return rawFiles.values.compactMap { $0 as? JSONPayload }.compactMap { File(json: $0) }
  }
}
