//
//  File.swift
//  GistCommenter
//
//  Created by Bruno Machado on 18/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation

struct File {
  
  private enum Keys: String {
    case content
    case fileName = "filename"
    case language
    case truncated
    case type
    case url = "raw_url"
  }
  
  let fileName: String
  let type: String
  let language: String
  let url: String
  let truncated: Bool
  let content: String
}

extension File: Serializable {
  
  init?(json: JSONPayload) {
    guard let fileName = json[Keys.fileName.rawValue] as? String,
      let type = json[Keys.type.rawValue] as? String,
      let url = json[Keys.url.rawValue] as? String else {
        return nil
    }
    
    self.content = json[Keys.content.rawValue] as? String ?? ""
    self.fileName = fileName
    self.language = json[Keys.language.rawValue] as? String ?? "Text"
    self.truncated = json[Keys.truncated.rawValue] as? Bool ?? false
    self.type = type
    self.url = url
  }
}
