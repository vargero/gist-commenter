//
//  GistDetailView.swift
//  GistCommenter
//
//  Created by Bruno Machado on 19/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift

class GistDetailView: UIView {
  
  private let disposeBag = DisposeBag()
  
  private let gistInformationView = UIView()
  private let userNameLabel = UILabel()
  private let createdLabel = UILabel()
  private let descriptionLabel = UILabel()
  private let avatarView = UIImageView()
  public let filesTableView = UITableView(frame: .zero, style: .plain)
  
  private let commentHeaderView = UIView()
  public let commentContainerView = UIView()
  private let commentCountLabel = UILabel()
  private let commentCountIcon: UIImageView = UIImageView(image: #imageLiteral(resourceName: "messageIcon"))
  public var isShowingCommentContainerVariable = Variable<Bool>(false)
  
  public var isShowingCommentContainer: Bool {
    get {
      return isShowingCommentContainerVariable.value
    }
    set {
      isShowingCommentContainerVariable.value = newValue
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupView()
    isShowingCommentContainerVariable.asDriver().drive(onNext: { [weak self] showing in
      self?.showCommentContainer(showing)
    }).disposed(by: disposeBag)
  }
  
  @available(*, unavailable)
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func setupView() {
    addSubview(gistInformationView)
    addSubview(filesTableView)
    addSubview(commentHeaderView)
    addSubview(commentContainerView)
    
    backgroundColor = .lightGray
    filesTableView.backgroundColor = .clear
    
    gistInformationView.snp.makeConstraints { make in
      make.left.right.equalToSuperview()
      make.top.equalTo(self.safeAreaLayoutGuide)
    }
    
    filesTableView.sectionFooterHeight = 10.0
    filesTableView.snp.makeConstraints { make in
      make.left.equalToSuperview().offset(10)
      make.right.equalToSuperview().offset(-10)
      make.bottom.equalTo(self.safeAreaLayoutGuide.snp.bottom).offset(-40)
      make.top.equalTo(gistInformationView.snp.bottom).offset(10)
    }
    
    commentHeaderView.snp.makeConstraints { make in
      make.left.right.equalToSuperview()
      make.bottom.equalToSuperview()
      make.top.equalTo(self.safeAreaLayoutGuide.snp.bottom).offset(-30)
    }
    
    commentContainerView.snp.makeConstraints { make in
      make.left.right.equalToSuperview()
      make.top.equalTo(commentHeaderView.snp.bottom)
    }
    
    setupGistInformationView()
    setupCommentHeaderView()
  }
  
  private func setupGistInformationView() {
    gistInformationView.addSubview(userNameLabel)
    gistInformationView.addSubview(avatarView)
    gistInformationView.addSubview(descriptionLabel)
    gistInformationView.addSubview(createdLabel)
    
    gistInformationView.backgroundColor = .white
    
    makeShadow(offset: CGSize(width: 0.0, height: 2.0))
    
    userNameLabel.textColor = .highlightBlue
    userNameLabel.font = UIFont.systemFont(ofSize: 16.0, weight: .medium)
    
    createdLabel.textColor = .textGray
    createdLabel.font = UIFont.systemFont(ofSize: 12.0, weight: .light)
    
    descriptionLabel.font = UIFont.systemFont(ofSize: 12.0, weight: .light)
    descriptionLabel.numberOfLines = 0
    
    avatarView.snp.makeConstraints { make in
      make.left.equalToSuperview().offset(5)
      make.top.equalToSuperview().offset(5)
      make.height.width.equalTo(40)
    }
    
    avatarView.layer.cornerRadius = 20
    avatarView.layer.masksToBounds = true
    
    userNameLabel.snp.makeConstraints { make in
      make.top.equalTo(avatarView)
      make.right.equalToSuperview().offset(-5)
      make.leading.equalTo(avatarView.snp.trailing).offset(5)
    }
    
    createdLabel.snp.makeConstraints { make in
      make.bottom.equalTo(avatarView)
      make.right.equalToSuperview().offset(-5)
      make.left.equalTo(userNameLabel)
    }
    
    descriptionLabel.snp.makeConstraints { make in
      make.right.equalToSuperview().offset(-5)
      make.left.equalToSuperview().offset(5)
      make.top.equalTo(avatarView.snp.bottom).offset(5)
      make.bottom.equalToSuperview().offset(-5)
    }
  }
  
  private func setupCommentHeaderView() {
    makeShadow(offset: CGSize(width: 0.0, height: -2.0))
    
    commentHeaderView.backgroundColor = .white
    commentContainerView.backgroundColor = .lightGray
    
    let tapGesture = UITapGestureRecognizer()
    commentHeaderView.addGestureRecognizer(tapGesture)
    
    tapGesture.rx.event.bind(onNext: { [weak self] recognizer in
      guard let me = self else { return }
      me.isShowingCommentContainer = !me.isShowingCommentContainer
    }).disposed(by: disposeBag)
    
    commentHeaderView.addSubview(commentCountIcon)
    commentHeaderView.addSubview(commentCountLabel)
    
    commentCountLabel.font = UIFont.systemFont(ofSize: 12.0)
    commentCountLabel.textAlignment = .right
    
    commentCountIcon.snp.makeConstraints { make in
      make.right.equalToSuperview().offset(-10)
      make.top.equalToSuperview().offset(5)
      make.height.width.equalTo(15)
    }
    
    commentCountLabel.snp.makeConstraints { make in
      make.right.equalTo(commentCountIcon.snp.left).offset(-5)
      make.bottom.equalTo(commentCountIcon)
    }
  }
  
  private func showCommentContainer(_ show: Bool) {
    if show {
      commentHeaderView.snp.remakeConstraints { make in
        make.left.right.equalToSuperview()
        make.top.equalTo(self.safeAreaLayoutGuide).offset(5)
        make.height.equalTo(30)
      }
      
      commentContainerView.snp.makeConstraints { make in
        make.left.bottom.right.equalToSuperview()
        make.top.equalTo(commentHeaderView.snp.bottom)
      }
    } else {
      commentHeaderView.snp.remakeConstraints { make in
        make.left.right.equalToSuperview()
        make.bottom.equalToSuperview()
        make.top.equalTo(self.safeAreaLayoutGuide.snp.bottom).offset(-30)
      }
      
      commentContainerView.snp.makeConstraints { make in
        make.left.right.equalToSuperview()
        make.top.equalTo(commentHeaderView.snp.bottom)
      }
    }
    
    UIView.animate(withDuration: 0.5) { [weak self] in
      self?.layoutIfNeeded()
    }
  }
  
  func setup(with gist: GistViewModel) {
    userNameLabel.text = gist.userName
    descriptionLabel.text = gist.description
    createdLabel.text = gist.creationDate
    commentCountLabel.text = "\(gist.comments.count) comments"
    avatarView.kf.setImage(with: gist.avatarURL)
  }
}
