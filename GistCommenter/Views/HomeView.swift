//
//  HomeView.swift
//  GistCommenter
//
//  Created by Bruno Machado on 20/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import SnapKit
import UIKit

class HomeView: UIView {
  
  private let footerView = GistCollectionFooterView()
  private let menuContainer = UIView()
  public let button = UIButton(type: .custom)
  private let buttonBackground = UIView()
  private let cameraIcon = UIImageView(image: #imageLiteral(resourceName: "cameraIcon"))
  
  public let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupView()
  }
  
  @available(*, unavailable)
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func setupView() {
    backgroundColor = .white
    
    addSubview(footerView)
    addSubview(collectionView)
    addSubview(menuContainer)
    addSubview(buttonBackground)
    addSubview(button)
    button.addSubview(cameraIcon)
    
    button.backgroundColor = .highlightBlue
    button.layer.cornerRadius = 30
    button.layer.masksToBounds = true
    
    cameraIcon.image = cameraIcon.image!.withRenderingMode(.alwaysTemplate)
    cameraIcon.tintColor = .white
    
    buttonBackground.backgroundColor = .lightGray
    buttonBackground.layer.cornerRadius = 35
    buttonBackground.layer.masksToBounds = true
    
    menuContainer.makeShadow(offset: CGSize(width: 0, height: -2.0))
    
    menuContainer.snp.makeConstraints { make in
      make.bottom.left.right.equalToSuperview()
      make.top.equalTo(self.safeAreaLayoutGuide.snp.bottom).offset(-50)
    }
    
    collectionView.snp.makeConstraints { make in
      make.top.left.right.equalToSuperview()
      make.bottom.equalTo(menuContainer.snp.top)
    }
    
    button.snp.makeConstraints { make in
      make.centerX.equalToSuperview()
      make.centerY.equalTo(menuContainer.snp.top)
      make.height.width.equalTo(60)
    }
    
    buttonBackground.snp.makeConstraints { make in
      make.centerX.equalToSuperview()
      make.centerY.equalTo(menuContainer.snp.top)
      make.height.width.equalTo(70)
    }
    
    cameraIcon.snp.makeConstraints { make in
      make.center.equalToSuperview()
      make.height.width.equalTo(30)
    }
    
    collectionView.backgroundColor = .lightGray
    collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
  }
}
