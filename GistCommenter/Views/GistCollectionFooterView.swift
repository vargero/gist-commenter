//
//  GistCollectionFooterView.swift
//  GistCommenter
//
//  Created by Bruno Machado on 20/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import SnapKit
import UIKit

class GistCollectionFooterView: UIView {
  
  private let loadMoreLabel = UILabel()
  private let loadingView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupView()
  }
  
  @available(*, unavailable)
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func setupView() {
    addSubview(loadMoreLabel)
    addSubview(loadingView)
    
    loadingView.isHidden = true
    
    backgroundColor = .lightGray
    
    layer.borderColor = UIColor.textGray.cgColor
    layer.borderWidth = 1
    
    loadMoreLabel.text = "LOAD MORE"
    loadMoreLabel.textAlignment = .center
    loadMoreLabel.textColor = .textGray
    
    loadMoreLabel.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
    
    loadingView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
  
  func start() {
    loadMoreLabel.isHidden = true
    loadingView.isHidden = false
    loadingView.startAnimating()
  }
  
  func stop() {
    loadMoreLabel.isHidden = false
    loadingView.isHidden = true
    loadingView.stopAnimating()
  }
}
