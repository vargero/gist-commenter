//
//  GistCommentTableViewCell.swift
//  GistCommenter
//
//  Created by Bruno Machado on 19/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import SnapKit
import UIKit

class GistCommentTableViewCell: UITableViewCell {
  
  private let userNameLabel = UILabel()
  private let creationDateLabel = UILabel()
  private let commentBodyTextView = UITextView()
  private let containerView = UIView()
  private let avatarView = UIImageView()
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupView()
  }
  
  @available(*, unavailable)
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    userNameLabel.text = nil
    commentBodyTextView.text = nil
    
    avatarView.kf.cancelDownloadTask()
  }
  
  func setup(with comment: CommentViewModel) {
    userNameLabel.text = comment.userName
    creationDateLabel.text = comment.creationDate
    commentBodyTextView.text = comment.body
    commentBodyTextView
      .heightAnchor
      .constraint(equalToConstant: commentBodyTextView.sizeThatFits(self.bounds.size).height)
      .isActive = true
    avatarView.kf.setImage(with: comment.avatarURL)
  }
  
  private func setupView() {
    makeShadow()
    backgroundColor = .clear
    
    commentBodyTextView.isScrollEnabled = false
    commentBodyTextView.isSelectable = false
    
    addSubview(containerView)
    containerView.addSubview(avatarView)
    containerView.addSubview(userNameLabel)
    containerView.addSubview(creationDateLabel)
    containerView.addSubview(commentBodyTextView)
    
    userNameLabel.textColor = .highlightBlue
    userNameLabel.font = UIFont.systemFont(ofSize: 15.0, weight: .medium)
    
    creationDateLabel.textColor = .textGray
    creationDateLabel.font = UIFont.systemFont(ofSize: 12.0, weight: .light)
    
    commentBodyTextView.font = UIFont.systemFont(ofSize: 12.0)
    
    containerView.backgroundColor = .white
    
    containerView.snp.makeConstraints { make in
      make.left.equalToSuperview().offset(10)
      make.right.equalToSuperview().offset(-10)
      make.top.equalToSuperview().offset(5)
      make.bottom.equalToSuperview().offset(-5)
    }
    
    avatarView.snp.makeConstraints { make in
      make.left.equalToSuperview().offset(5)
      make.top.equalToSuperview().offset(5)
      make.height.width.equalTo(30)
    }
    
    avatarView.layer.cornerRadius = 15
    avatarView.layer.masksToBounds = true
    
    userNameLabel.snp.makeConstraints { make in
      make.top.equalTo(avatarView)
      make.right.equalToSuperview().offset(-40)
      make.leading.equalTo(avatarView.snp.trailing).offset(5)
    }
    
    creationDateLabel.snp.makeConstraints { make in
      make.bottom.equalTo(avatarView)
      make.right.equalToSuperview().offset(-5)
      make.left.equalTo(userNameLabel)
    }
    
    commentBodyTextView.snp.makeConstraints { make in
      make.top.equalTo(avatarView.snp.bottom).offset(5)
      make.left.equalToSuperview().offset(5)
      make.right.bottom.equalToSuperview().offset(-5)
    }
  }
}
