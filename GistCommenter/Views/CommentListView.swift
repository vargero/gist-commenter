//
//  CommentListView.swift
//  GistCommenter
//
//  Created by Bruno Machado on 19/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import RxCocoa
import RxSwift
import SnapKit
import UIKit

class CommentListView: UIView {
  
  private let commentFieldContainer = UIView()
  public let commentField = UITextView()
  public let submitCommentButton = UIButton(type: .system)
  
  public let commentTableView = UITableView(frame: .zero, style: .plain)
  
  private let disposeBag = DisposeBag()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupView()
    setupBinding()
  }
  
  @available(*, unavailable)
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func setupView() {
    addSubview(commentTableView)
    addSubview(commentFieldContainer)
    
    commentTableView.backgroundColor = .clear
    commentTableView.separatorStyle = .none
    
    commentFieldContainer.snp.makeConstraints { make in
      make.top.left.right.equalToSuperview()
      make.height.equalTo(50)
    }
    
    commentTableView.snp.makeConstraints { make in
      make.left.right.bottom.equalToSuperview()
      make.top.equalTo(commentFieldContainer.snp.bottom).offset(10)
    }
    
    setupCommentFieldContainer()
  }
  
  private func setupCommentFieldContainer() {
    commentFieldContainer.backgroundColor = .white
    commentFieldContainer.makeShadow(offset: CGSize(width: 0.0, height: 2.0))
    
    commentFieldContainer.addSubview(commentField)
    commentFieldContainer.addSubview(submitCommentButton)
    
    submitCommentButton.setTitle("Submit", for: .normal)
    
    commentField.layer.cornerRadius = 5
    commentField.layer.masksToBounds = true
    commentField.layer.borderColor = UIColor.black.cgColor
    commentField.layer.borderWidth = 1.0
    
    submitCommentButton.snp.makeConstraints { make in
      make.right.equalToSuperview().offset(-10)
      make.top.equalToSuperview().offset(5)
      make.bottom.equalToSuperview().offset(-5)
      make.width.equalTo(50)
    }
    
    commentField.snp.makeConstraints { make in
      make.top.bottom.equalTo(submitCommentButton)
      make.left.equalToSuperview().offset(10)
      make.right.equalTo(submitCommentButton.snp.left).offset(-10)
    }
  }
  
  private func setupBinding() {
    commentField.rx.text.asObservable().subscribe(onNext: { [weak self] text in
      self?.submitCommentButton.isEnabled = !(text == nil || text == "")
    }).disposed(by: disposeBag)
    
    let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
    
    addGestureRecognizer(gestureRecognizer)
  }
  
  @objc private func dismissKeyboard() {
    commentField.resignFirstResponder()
  }
}
