//
//  FileSectionHeaderView.swift
//  GistCommenter
//
//  Created by Bruno Machado on 19/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import SnapKit
import UIKit

class FileSectionHeaderView: UIView {
  
  private let fileNameLabel = UILabel()
  private let fileHeaderView = UIView()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupView()
  }
  
  @available(*, unavailable)
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func setupView() {
    backgroundColor = .white
    addSubview(fileHeaderView)
    fileHeaderView.addSubview(fileNameLabel)
    
    fileNameLabel.font = UIFont.boldSystemFont(ofSize: 14.0)
    fileNameLabel.textColor = .textGray
    
    fileHeaderView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
      make.height.equalTo(30)
    }
    
    fileNameLabel.snp.makeConstraints { make in
      make.left.top.equalToSuperview().offset(5)
      make.right.bottom.equalToSuperview().offset(-5)
    }
  }
  
  func setup(with fileName: String) {
    fileNameLabel.text = fileName
  }
}
