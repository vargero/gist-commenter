//
//  FileCollectionViewCell.swift
//  GistCommenter
//
//  Created by Bruno Machado on 19/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import SnapKit
import UIKit

class FileCollectionViewCell: UITableViewCell {
  
  private let maximunViewHeight: CGFloat = 500.0
  
  private let fileView = HighlightTextView()
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupView()
  }
  
  @available(*, unavailable)
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func prepareForReuse() {
    fileView.text = nil
  }
  
  private func setupView() {
    makeShadow()
    backgroundColor = .clear
    
    fileView.isSelectable = false
    
    addSubview(fileView)
    fileView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }
  
  func setup(with file: FileViewModel) {
    fileView.text = file.content
    
    let sizeThatFits = fileView.sizeThatFits(self.bounds.size).height
    let textViewHeight = sizeThatFits <= maximunViewHeight ? sizeThatFits : maximunViewHeight
    fileView.heightAnchor.constraint(equalToConstant: textViewHeight).isActive = true
  }
}
