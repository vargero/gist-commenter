//
//  GistCollectionViewCell.swift
//  GistCommenter
//
//  Created by Bruno Machado on 18/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import UIKit
import SnapKit
import Kingfisher

class GistCollectionViewCell: UICollectionViewCell {
  
  private let highlightView = HighlightTextView()
  private let createdLabel = UILabel()
  private let titleLabel = UILabel()
  private let avatarView = UIImageView()
  private let commentCountLabel = UILabel()
  private let commentCountIcon: UIImageView = UIImageView(image: #imageLiteral(resourceName: "messageIcon"))
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    highlightView.isScrollEnabled = false
    highlightView.isSelectable = false
    setupView()
  }
  
  @available(*, unavailable)
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func setupView() {
    makeShadow()
    backgroundColor = .white
    
    addSubview(highlightView)
    addSubview(createdLabel)
    addSubview(avatarView)
    addSubview(titleLabel)
    addSubview(commentCountLabel)
    addSubview(commentCountIcon)
    
    highlightView.isUserInteractionEnabled = false
    
    highlightView.clipsToBounds = true
    createdLabel.clipsToBounds = true
    avatarView.clipsToBounds = true
    
    titleLabel.textColor = .highlightBlue
    titleLabel.font = UIFont.systemFont(ofSize: 13.0, weight: .medium)
    
    createdLabel.textColor = .textGray
    createdLabel.font = UIFont.systemFont(ofSize: 12.0, weight: .light)
    
    commentCountLabel.font = UIFont.systemFont(ofSize: 12.0)
    commentCountLabel.textAlignment = .right
    
    highlightView.snp.makeConstraints { make in
      make.bottom.left.right.equalToSuperview()
      make.height.equalTo(200)
    }
    
    avatarView.snp.makeConstraints { make in
      make.left.equalToSuperview().offset(5)
      make.top.equalToSuperview().offset(5)
      make.height.width.equalTo(40)
    }
    
    avatarView.layer.cornerRadius = 20
    avatarView.layer.masksToBounds = true
    
    commentCountIcon.snp.makeConstraints { make in
      make.height.width.equalTo(15)
      make.top.equalToSuperview().offset(5)
      make.right.equalToSuperview().offset(-5)
    }
    
    commentCountLabel.snp.makeConstraints { make in
      make.right.equalTo(commentCountIcon.snp.left).offset(-5)
      make.centerY.equalTo(commentCountIcon.snp.centerY)
    }
    
    titleLabel.snp.makeConstraints { make in
      make.top.equalTo(avatarView)
      make.right.equalToSuperview().offset(-40)
      make.leading.equalTo(avatarView.snp.trailing).offset(5)
    }
    
    createdLabel.snp.makeConstraints { make in
      make.bottom.equalTo(avatarView)
      make.right.equalToSuperview().offset(-5)
      make.left.equalTo(titleLabel)
    }
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    
    highlightView.text = nil
    createdLabel.text = nil
    titleLabel.text = nil
    commentCountLabel.text = nil
    avatarView.kf.cancelDownloadTask()
  }
  
  func setup(with gistViewModel: GistCollectionViewModel) {
    highlightView.text = gistViewModel.fileText
    createdLabel.text = gistViewModel.creationDate
    titleLabel.text = gistViewModel.title
    avatarView.kf.setImage(with: gistViewModel.avatarURL)
    commentCountLabel.text = gistViewModel.commentCount.description
  }
}
