//
//  FileView.swift
//  GistCommenter
//
//  Created by Bruno Machado on 18/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Highlightr
import UIKit

class HighlightTextView: UITextView {
  private var highlighter = Highlightr()
  private let textView = UITextView()
  private var internalText: String?
  
  override var text: String? {
    get {
      return internalText
    }
    set {
      internalText = newValue
      attributedText = highlighter?.highlight(newValue ?? "", fastRender: true)
    }
  }
  
  override init(frame: CGRect, textContainer: NSTextContainer?) {
    super.init(frame: frame, textContainer: textContainer)
    highlighter?.setTheme(to: "paraiso-dark")
    backgroundColor = highlighter?.theme.themeBackgroundColor ?? .black
  }
  
  @available(*, unavailable)
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
