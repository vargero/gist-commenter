//
//  DefaultsManager.swift
//  GistCommenter
//
//  Created by Bruno Machado on 20/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation

class DefaultsManager {

  private enum Keys: String {
    case gistListKey
  }
  
  static let shared = DefaultsManager()
  private let manager = UserDefaults.standard
  
  init() {
    
  }
  
  var gistList: [String] {
    get {
      guard let result = manager.array(forKey: Keys.gistListKey.rawValue) as? [String] else { return [] }
      return result
    }
    set {
      manager.setValue(newValue, forKey: Keys.gistListKey.rawValue)
      manager.synchronize()
    }
  }
  
  func appendNewGist(with gistID: String) {
    var array = gistList
    guard !array.contains(gistID) else { return }
    array.append(gistID)
    gistList = array
  }
}
