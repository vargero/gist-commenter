//
//  Extensions.swift
//  GistCommenter
//
//  Created by Bruno Machado on 19/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation
import UIKit

extension DateFormatter {
  
  public static var standardDateFormatter: DateFormatter {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    
    return formatter
  }
}

extension UIColor {
  
  static let lightGray = UIColor(red: 232.0/255.0, green: 232.0/255.0, blue: 238.0/255.0, alpha: 1)
  static let highlightBlue = UIColor(red: 44.0/255.0, green: 63.0/255.0, blue: 168.0/255.0, alpha: 1)
  static let textGray = UIColor(red: 89.0/255.0, green: 89.0/255.0, blue: 89.0/255.0, alpha: 1)
}

extension UIView {
  
  func makeShadow(color: UIColor = .black,
                  shadowRadius: CGFloat = 2.0,
                  shadowOpacity: Float = 0.3,
                  offset: CGSize = CGSize(width: 2.0, height: 2.0)) {
    layer.shadowColor = color.cgColor
    layer.shadowRadius = shadowRadius
    layer.shadowOpacity = shadowOpacity
    layer.shadowOffset = offset
  }
}

extension String {
  
  func truncate(length: Int, trailing: String = "…") -> String {
    if self.characters.count > length {
      return String(self.characters.prefix(length)) + trailing
    } else {
      return self
    }
  }
}

extension Date {
  
  func beautifulDate() -> String {
    let dateFormatter = DateFormatter()
    
    if NSCalendar.current.isDateInToday(self) {
      dateFormatter.dateFormat = "HH:mm"
      
      return "Created at \(dateFormatter.string(from: self))"
    } else {
      dateFormatter.dateFormat = "MMM dd, yyyy"
      
      return "Created on \(dateFormatter.string(from: self))"
    }
  }
}

