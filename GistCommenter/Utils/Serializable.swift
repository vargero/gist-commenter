//
//  Serializable.swift
//  GistCommenter
//
//  Created by Bruno Machado on 18/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation

//
//  JSONMappable.swift
//  Pods
//
//  Created by Won, Charles on 8/1/16.
//  Copyright © 2016 Disney ABC Television Group. All rights reserved.
//

import Foundation

public typealias JSONPayload = [String: Any]

public protocol Serializable {
  
  init?(json: JSONPayload)
  
  func serialized() -> JSONPayload?
}

public extension Serializable {
  
  public func serialized() -> JSONPayload? {
    return nil
  }
}
