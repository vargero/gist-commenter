//
//  AppDelegate.swift
//  GistCommenter
//
//  Created by Bruno Machado on 18/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  // MARK: - Properties
  
  var window: UIWindow?
  var coordinator: AppCoordinator?
  
  // MARK: - Application Lifecycle
  
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.rootViewController = UINavigationController()
    coordinator = AppCoordinator(with: window?.rootViewController as! UINavigationController)
    coordinator?.start()
    window?.makeKeyAndVisible()
    
    return true
  }
}

