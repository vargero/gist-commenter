//
//  GistService.swift
//  GistCommenter
//
//  Created by Bruno Machado on 18/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Alamofire
import RxAlamofire
import RxSwift

enum ServiceConstants: String {
  case baseURL = "https://api.github.com/gists"
  case comments
  case publicGists = "public"
  case starred
}

enum GistServiceError: Error {
  case invalidResponse
}

class GistService {
  
  private let sessionManager: SessionManager
  private let disposeBag = DisposeBag()
  static let shared = GistService()
  
  var headers: [String: String] = {
    return ["Authorization": "token f09ce2967d12248d99dd8d8931e1bb0b71b9a898"]
  }()
  
  init() {
    let configuration = URLSessionConfiguration.default
    configuration.urlCache = nil
    self.sessionManager = Alamofire.SessionManager(configuration: configuration)
  }
  
  func requestGistList(starred: Bool = false, page: Int = 1, itemsPerPage: Int = 10) -> Observable<[Gist]> {
    let directory = starred ? ServiceConstants.starred.rawValue : ServiceConstants.publicGists.rawValue
    let queryString = "page=\(page)&per_page=\(itemsPerPage)"
    let url = "\(ServiceConstants.baseURL.rawValue)/\(directory)?\(queryString)"

    return json(.get, url, headers: headers).flatMap { data -> Observable<[Gist]> in
      guard let data = data as? [JSONPayload] else {
        return Observable.create({ observer -> Disposable in
          observer.onError(GistServiceError.invalidResponse)
          observer.onCompleted()
          return Disposables.create()
        })
      }
      
      return Observable.from(data.compactMap { $0["id"] as? String }.compactMap { id in
        return GistService.shared.requestGist(with: id)
      }).merge().toArray()
    }
  }
  
  func requestGist(with gistID: String) -> Observable<Gist> {
    let url = "\(ServiceConstants.baseURL.rawValue)/\(gistID)"
    
    return json(.get, url, headers: headers).flatMap({ data -> Observable<Gist> in
      guard let data = data as? JSONPayload,
        let gist = Gist(json: data) else {
          return Observable.create({ observer -> Disposable in
            observer.onCompleted()
            return Disposables.create()
          })
      }
      
      return Observable.create({ observer -> Disposable in
        observer.onNext(gist)
        observer.onCompleted()
        return Disposables.create()
      })
    })
  }
  
  func requestGistComments(with gistID: String) -> Observable<[Comment]> {
    let url = "\(ServiceConstants.baseURL.rawValue)/\(gistID)/\(ServiceConstants.comments.rawValue)"
    
    return json(.get, url, headers: headers).flatMap({ data -> Observable<[Comment]> in
      guard let data = data as? [JSONPayload] else {
        return Observable.create({ observer -> Disposable in
          observer.onError(GistServiceError.invalidResponse)
          observer.onCompleted()
          return Disposables.create()
        })
      }
      let comments = data.compactMap { Comment(json: $0) }
      return Observable.create({ observer -> Disposable in
        observer.onNext(comments)
        observer.onCompleted()
        return Disposables.create()
      })
    })
  }
  
  func sendGistComment(with gistID: String, body: String) -> Observable<Comment> {
    let url = "\(ServiceConstants.baseURL.rawValue)/\(gistID)/\(ServiceConstants.comments.rawValue)"
    let parameters = ["body": body]
    return json(.post, url, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers)
      .flatMap({ data -> Observable<Comment> in
        
        guard let data = data as? JSONPayload, let comment = Comment(json: data) else {
          return Observable.create({ observer -> Disposable in
            observer.onError(GistServiceError.invalidResponse)
            observer.onCompleted()
            return Disposables.create()
          })
        }
        return Observable.create({ observer -> Disposable in
          observer.onNext(comment)
          observer.onCompleted()
          return Disposables.create()
        })
      })
  }
}
