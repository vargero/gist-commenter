//
//  HomeCoordinator.swift
//  GistCommenter
//
//  Created by Bruno Machado on 18/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import RxSwift
import UIKit

class HomeCoordinator: ParentCoordinator {
  
  private let disposeBag = DisposeBag()
  private let navigationController: UINavigationController!
  private let homeViewController: HomeViewController!
  var childCoordinators: [Coordinator] = []
  
  init(with navigationController: UINavigationController) {
    self.navigationController = navigationController
    homeViewController = HomeViewController()
    homeViewController.delegate = self
  }
  
  func start() {
    navigationController.show(homeViewController, sender: nil)
  }
  
  func openGist(with gistId: String) {
    let coordinator = GistDetailCoordinator(with: navigationController)
    add(child: coordinator)
    coordinator.start(with: gistId) { [weak self] coordinator in
      self?.remove(child: coordinator)
    }
  }
  
  func openCamera() {
    let coordinator = CameraCoordinator(with: navigationController)
    add(child: coordinator)
    coordinator.start { [weak self] (coordinator, gistId) in
      guard let gistId = gistId else { return }
      self?.openGist(with: gistId)
    }
  }
}

extension HomeCoordinator: HomeViewControllerDelegate {
  
  func viewControllerDidSelectGist(_ viewController: HomeViewController, with gistId: String) {
    openGist(with: gistId)
  }
  
  func viewControllerDidTapCameraButton(_ viewController: HomeViewController) {
    openCamera()
  }
}
