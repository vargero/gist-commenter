//
//  GistDetailCoordinator.swift
//  GistCommenter
//
//  Created by Bruno Machado on 19/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import RxSwift
import UIKit

class GistDetailCoordinator: ParentCoordinator {
  typealias Handler = (ParentCoordinator) -> Void
  
  private let disposeBag = DisposeBag()
  private let navigationController: UINavigationController!
  private var viewController: GistDetailViewController?
  private var didComplete: Handler?
  
  var childCoordinators: [Coordinator] = []
  
  init(with navigationController: UINavigationController) {
    self.navigationController = navigationController
  }
  
  func start(with gistId: String, didComplete: Handler? = nil) {
    self.didComplete = didComplete
    
    GistService.shared.requestGist(with: gistId).subscribe(onNext: { [weak self] gist in
      guard let me = self else { return }
      DefaultsManager.shared.appendNewGist(with: gistId)
      let viewController = GistDetailViewController(viewModel: GistViewModel(with: gist))
      me.viewController = viewController
      me.navigationController.show(viewController, sender: nil)
    }).disposed(by: disposeBag)
  }
  
  func end() {
    viewController?.presentingViewController?.dismiss(animated: true, completion: { [weak self] in
      guard let me = self else { return }
      me.didComplete?(me)
    })
  }
}
