//
//  AppCoordinator.swift
//  GistCommenter
//
//  Created by Bruno Machado on 18/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation

import UIKit

class AppCoordinator: ParentCoordinator {
  
  private let navigationController: UINavigationController!
  
  var childCoordinators: [Coordinator] = []
  
  init(with navigationController: UINavigationController) {
    self.navigationController = navigationController
  }
  
  func start() {
    let homeCoordinator = HomeCoordinator(with: navigationController)
    homeCoordinator.start()
    childCoordinators.append(homeCoordinator)
  }
}
