//
//  Coordinator.swift
//  GistCommenter
//
//  Created by Bruno Machado on 18/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import Foundation

protocol Coordinator: class {}

protocol ParentCoordinator: Coordinator {
  var childCoordinators: [Coordinator] { get set }
  func add(child coordinator: Coordinator)
  func remove(child coordinator: Coordinator)
}

extension ParentCoordinator {
  func add(child coordinator: Coordinator) {
    childCoordinators.append(coordinator)
  }
  
  func remove(child coordinator: Coordinator) {
    if let index = childCoordinators.index(where: { $0 === coordinator }) {
      childCoordinators.remove(at: index)
    }
  }
}
