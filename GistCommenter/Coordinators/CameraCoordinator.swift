//
//  CameraCoordinator.swift
//  GistCommenter
//
//  Created by Bruno Machado on 20/08/18.
//  Copyright © 2018 brunoMachado. All rights reserved.
//

import AVFoundation
import Foundation
import QRCodeReader
import UIKit

class CameraCoordinator: ParentCoordinator {
  typealias Handler = (CameraCoordinator, String?) -> Void
  
  lazy var viewController: QRCodeReaderViewController = {
    let builder = QRCodeReaderViewControllerBuilder {
      $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
    }
    
    return QRCodeReaderViewController(builder: builder)
  }()
  
  private let navigationController: UINavigationController!
  private var didComplete: Handler?
  
  var childCoordinators: [Coordinator] = []
  
  init(with navigationController: UINavigationController) {
    self.navigationController = navigationController
    
    viewController.delegate = self
  }
  
  func start(didComplete: Handler? = nil) {
    self.didComplete = didComplete
    switch AVCaptureDevice.authorizationStatus(for: .video) {
    case .authorized:
      showViewController()
    case .notDetermined:
      requestCameraAccess()
    default: end(with: "ec73c321d62d1b4eaf0f51ca478ccd92")
    }
  }
  
  private func showViewController() {
    navigationController.show(viewController, sender: nil)
  }
  
  private func requestCameraAccess() {
    AVCaptureDevice.requestAccess(for: .video) { [weak self] authorized in
      self?.showViewController()
    }
  }
  
  func end(with gistID: String? = nil) {
    navigationController.popViewController(animated: false)
    didComplete?(self, gistID)
  }
}

extension CameraCoordinator: QRCodeReaderViewControllerDelegate {
  
  func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
    var gistId = result.value
    if gistId.contains("http") {
      gistId = URL(string: gistId)!.lastPathComponent
    }
    
    end(with: gistId)
  }
  
  func readerDidCancel(_ reader: QRCodeReaderViewController) {
    end()
  }
}
